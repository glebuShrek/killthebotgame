package ru.fml239.surfaceviewexample.Utils;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Gleb on 24.04.2016.
 */
public class Values {
    public static int FIELD_WIDTH;
    public static int FIELD_HEIGHT;
    public static float PPU_X;
    public static float PPU_Y;
    public static HashMap<String, Bitmap> textures;

    public static float getBlockMinSize(){
        return Math.min(PPU_X, PPU_Y);
    }
    public static void loadGraphics(AssetManager assetManager) {
        textures = new HashMap<>();
        try {
            textures.put("tomat.png", BitmapFactory.decodeStream(assetManager.open("tomat.png")));
            textures.put("heart.png", BitmapFactory.decodeStream(assetManager.open("heart.png")));
            textures.put("battle.png", BitmapFactory.decodeStream(assetManager.open("battle.png")));
            textures.put("button.png", BitmapFactory.decodeStream(assetManager.open("button.png")));
            textures.put("battle1.png", BitmapFactory.decodeStream(assetManager.open("battle1.png")));
            textures.put("blue.png", BitmapFactory.decodeStream(assetManager.open("blue.png")));
            textures.put("wall.png", BitmapFactory.decodeStream(assetManager.open("wall.png")));
            textures.put("next.png", BitmapFactory.decodeStream(assetManager.open("next.png")));
            textures.put("boom.jpg", BitmapFactory.decodeStream(assetManager.open("boom.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

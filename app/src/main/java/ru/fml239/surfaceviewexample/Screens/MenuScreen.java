package ru.fml239.surfaceviewexample.Screens;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import ru.fml239.framework.scene.Stage.Screen;
import ru.fml239.surfaceviewexample.Game;
import ru.fml239.surfaceviewexample.Controller.Button;
import ru.fml239.surfaceviewexample.Utils.Values;

/**
 * Created by Gleb on 02.05.2016.
 */
public class MenuScreen extends Screen {
    private Bitmap img;
    private Button playButton;
    private Paint backgroundPaint;

    public MenuScreen(final Game game, SurfaceHolder surfaceHolder, int screenWidth, int screenHeight, AssetManager assetManager) {
        super(game, surfaceHolder, screenWidth, screenHeight);
        playButton = new Button(screenWidth / 3, screenHeight / 3, screenWidth, screenWidth, game, Values.textures.get("button.png")) {
            @Override
            public void act() {game.setScreen(ScreenType.GameScreen);
            }
        };
        inputProcessor.addToInterface(playButton);
        backgroundPaint=new Paint();
        backgroundPaint.setColor(Color.WHITE);
    }

    @Override
    protected void draw(Canvas canvas) {

        canvas.drawRect(0, 0, screenWidth, screenHeight, backgroundPaint);
        playButton.draw(canvas);
    }

    @Override
    protected void update(double deltaT) {

    }

    @Override
    public boolean isTouched() {
        return false;
    }

    @Override
    public boolean touchDown(float x, float y) {
        playButton.touchDown(x, y);
        if (playButton.isTouched())
            game.setScreen(ScreenType.GameScreen);
        return false;
    }

    @Override
    public boolean touchDrag(float x, float y) {
        return false;
    }

    @Override
    public boolean touchUp(float x, float y) {
        return false;
    }
}

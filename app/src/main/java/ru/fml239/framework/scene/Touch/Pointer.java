package ru.fml239.framework.scene.Touch;

import android.view.MotionEvent;

import ru.fml239.framework.math.Vector2;

/**
 * Created by Gleb on 04.05.2016.
 */
public class Pointer extends Vector2 {
    private int action;

    public Pointer(MotionEvent event) {
        int pointerIndex = event.getActionIndex();
        x = event.getX(pointerIndex);
        y = event.getY(pointerIndex);
        action = event.getAction();
    }

    public void move(MotionEvent event) {
        int pointerIndex = event.getActionIndex();
        setCoords(event.getX(pointerIndex), event.getY(pointerIndex));
        action = event.getAction();
    }


    public int getAction() {
        return action;
    }
}

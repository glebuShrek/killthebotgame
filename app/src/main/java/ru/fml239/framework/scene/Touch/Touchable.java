package ru.fml239.framework.scene.Touch;

/**
 * Created by Gleb on 17.04.2016.
 */
public interface Touchable {
    boolean isTouched();

    boolean touchDown(float x, float y);

    boolean touchDrag(float x, float y);

    boolean touchUp(float x, float y);
}

package ru.fml239.framework.scene.Stage;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import java.util.HashMap;

import ru.fml239.framework.scene.Touch.InputProcessor;
import ru.fml239.framework.scene.Touch.Pointer;
import ru.fml239.framework.scene.Touch.Touchable;
import ru.fml239.surfaceviewexample.Game;

/**
 * Created by Gleb on 27.04.2016.
 */
public abstract class Screen extends Thread implements Touchable {

    private SurfaceHolder surfaceHolder;
    protected InputProcessor inputProcessor;
    protected Game game;
    private volatile boolean running;
    protected int screenWidth;
    protected int screenHeight;
    private double lastTime;
    private double currentTime;
    private double deltaT;

    public Screen(Game game, SurfaceHolder surfaceHolder, int screenWidth, int screenHeight) {
        this.game = game;
        this.surfaceHolder = surfaceHolder;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.running = false;
        this.inputProcessor = new InputProcessor(this);
    }

    public void run() {
        if (!running)
            return;
        Canvas canvas = null;
        lastTime = System.currentTimeMillis() / 1000.0; //текущее время жизни телефона в секундах
        currentTime = 0;
        deltaT = 0;

        while (running) {
            try {
                canvas = surfaceHolder.lockCanvas();
                if (canvas != null)
                    synchronized (surfaceHolder) {
                        currentTime = System.currentTimeMillis() / 1000.0;
                        deltaT = currentTime - lastTime;
                        lastTime = currentTime;
                        update(deltaT);
                        draw(canvas);
                    }
            } finally {
                if (canvas != null)
                    surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    protected abstract void draw(Canvas canvas);

    protected abstract void update(double deltaT);

    public boolean applyMotionEvent(MotionEvent event){
        inputProcessor.processMotionEvent(event);
        inputProcessor.act();
        return true;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

}

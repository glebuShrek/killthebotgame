package ru.fml239.surfaceviewexample.Model;

import android.content.res.AssetManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import ru.fml239.surfaceviewexample.Utils.Values;
import ru.fml239.framework.math.Vector2;
import ru.fml239.surfaceviewexample.View.Box;
import ru.fml239.framework.scene.Stage.Group;

/**
 * Created by Gleb on 14.04.2016.
 */
public class Field {
    private int[][] matrix;
    private Box[][] walls;
    private Group wallsGroup;

    public Field(String fileName, float screenWidth, float screenHeight, AssetManager assetManager) {
        try {
            initField(fileName, assetManager);
            Values.PPU_X = screenWidth / Values.FIELD_WIDTH;
            Values.PPU_Y = screenHeight / Values.FIELD_HEIGHT;
            wallsGroup = new Group();
            walls = new Box[Values.FIELD_HEIGHT][Values.FIELD_WIDTH];
            for (int i = 0; i < Values.FIELD_HEIGHT; i++)
                for (int j = 0; j < Values.FIELD_WIDTH; j++) {
                    if (matrix[i][j] == 1) {
                        walls[i][j] = new Box(j * Values.PPU_X, i * Values.PPU_Y, Values.PPU_X, Values.PPU_Y);
                        wallsGroup.addActor(walls[i][j]);
                    }else{
                        walls[i][j] = null;
                    }
                }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initField(String levelName, AssetManager assetManager) throws IOException {
        matrix = createField(levelName, assetManager);
    }

    private int countRows(String fileName, AssetManager assetManager) throws IOException { //считает количество строк в файле
        int result = 0;
        Scanner sc = new Scanner(assetManager.open(fileName));
        while (sc.hasNext()) {
            result++;
            sc.nextLine();
        }
        sc.close();
        return result;
    }

    private int countColumns(String fileName, AssetManager assetManager) throws IOException { //считает максимальную длину строки в файле
        int result = 0, tmp;
        Scanner sc = new Scanner(assetManager.open(fileName));
        while (sc.hasNext()) {
            tmp = sc.nextLine().length();
            result = tmp > result ? tmp : result;
        }
        sc.close();
        return result;
    }

    private int[][] createField(String fileName, AssetManager assetManager) throws IOException { //создает поле из файла
        Values.FIELD_WIDTH = countColumns(fileName, assetManager);
        Values.FIELD_HEIGHT = countRows(fileName, assetManager);
        int[][] result = new int[Values.FIELD_HEIGHT][Values.FIELD_WIDTH]; //задаются размеры массива
        for (int i = 0; i < result.length; i++)
            for (int j = 0; j < result[i].length; j++)
                result[i][j] = 0;

        String tmp;
        Scanner sc = new Scanner(assetManager.open(fileName));
        for (int j = 0; j < result.length; j++) {
            tmp = sc.nextLine();
            for (int i = 0; i < tmp.length(); i++)
                result[j][i] = tmp.charAt(i) - '0';
        }


        sc.close();
        //инверсия по игреку
        /*int[] tmprow;
        for (int i = 0; i < result.length / 2; i++) {
            tmprow = result[i];
            result[i] = result[result.length - i - 1];
            result[result.length - i - 1] = tmprow;
        }*/
        //конец инверсии

        return result;
    }

    public int getCell(int x, int y) {
        return matrix[y][x];
    }

    public Group getWalls() {
        return wallsGroup;
    }

    public void getNeighbours(Group neighboursGroup, Vector2 cell) {
        neighboursGroup.clearGroup();
        int x = (int) cell.x;
        int y = (int) cell.y;
        for (int i = Math.max(y - 1, 0); i <= Math.min(y + 1, Values.FIELD_HEIGHT-1); i++)
            for (int j = Math.max(x - 1, 0); j <= Math.min(x + 1, Values.FIELD_WIDTH-1); j++)
                neighboursGroup.addActor(walls[i][j]);
    }
}

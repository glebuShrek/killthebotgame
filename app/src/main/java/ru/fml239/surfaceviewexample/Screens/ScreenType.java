package ru.fml239.surfaceviewexample.Screens;

/**
 * Created by Gleb on 30.04.2016.
 */
public enum ScreenType {
    GameScreen, MenuScreen, TrailerScreen;
}

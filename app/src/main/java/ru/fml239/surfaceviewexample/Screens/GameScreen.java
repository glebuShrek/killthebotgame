package ru.fml239.surfaceviewexample.Screens;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import java.io.IOException;

import ru.fml239.framework.math.Overlapers;
import ru.fml239.framework.scene.Stage.Screen;
import ru.fml239.framework.scene.Touch.InputProcessor;
import ru.fml239.surfaceviewexample.Controller.Button;
import ru.fml239.surfaceviewexample.Controller.Trigger;
import ru.fml239.surfaceviewexample.Game;
import ru.fml239.surfaceviewexample.Model.Bot;
import ru.fml239.surfaceviewexample.Model.Bullet;
import ru.fml239.surfaceviewexample.Model.Field;
import ru.fml239.surfaceviewexample.Model.MainHero;
import ru.fml239.surfaceviewexample.Utils.Values;
import ru.fml239.framework.scene.Stage.Actor;
import ru.fml239.framework.scene.Stage.Group;
import ru.fml239.surfaceviewexample.Controller.JoyStick;

public class GameScreen extends Screen {

    private Paint fontPaint;
    private SurfaceHolder surfaceHolder;

    private Paint backgroundPaint;

    private int score;
    private double cd;

    private int maxSpawn;
    private int waveSize;
    private int killed;
    private int spawned;


    private MainHero hero;
    private Group heroNeighbours;

    private Field field;

    private Paint paint;

    private JoyStick controller;
    private Trigger trigger;
    private Button nextWeaponButton;
    private Bot bot;

    Group sceneObjects; // объекты, которые рисуются на сцене и не обрабатывают нажатия
    Group walls; // стены
    Group bots; // стены
    Group userInterface; //элементи игрового интерфейса - кнопки и джойстик
    Group bullets;

    public GameScreen(Game game, SurfaceHolder surfaceHolder, int screenWidth, int screenHeight, AssetManager assetManager) {
        super(game, surfaceHolder, screenWidth, screenHeight);
        this.surfaceHolder = surfaceHolder;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        nextWeaponButton = new Button(Values.PPU_X * 1.5f, Values.PPU_Y * 1.5f, screenWidth, screenWidth, game, Values.textures.get("next.png")) {
            @Override
            public void act() {
                hero.NextWeapon();
            }
        };
        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.WHITE);	fontPaint = new Paint(Paint.HINTING_OFF);
        fontPaint.setTextSize(screenHeight/10);
        fontPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        fontPaint.setColor(Color.BLACK);

        paint = new Paint();
        paint.setColor(Color.YELLOW);

        maxSpawn = 5;
        waveSize = 15;
        bots = new Group();
        //bots.addActor(new Bot(200, 300, 30));
        //  bot = new Bot(200, 300, 30);

        field = new Field("1.txt", screenWidth, screenHeight, assetManager);

        bullets = new Group();
        hero = new MainHero(Values.PPU_X * 2.5f, Values.PPU_Y * 2.5f,  (Values.getBlockMinSize()/2));
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(assetManager.open("1.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        hero.setBitmap(bitmap);
        hero.setBulletsGroup(bullets);
        hero.update(0);
        heroNeighbours = new Group();
        field.getNeighbours(heroNeighbours, hero.getCell());

        controller = new JoyStick(hero);
        controller.setSize(screenWidth/5, screenWidth/5);
        trigger = new Trigger(hero);
        trigger.setSize(screenWidth/5, screenWidth/5);

        controller.setPosition((float) (screenWidth*0.1), (float) (screenHeight*0.8));
        trigger.setPosition((float) (screenWidth*0.8), (float) (screenHeight*0.8));

        walls = field.getWalls();

        sceneObjects = new Group();

        sceneObjects.addActor(hero);
        sceneObjects.addActor(bot);

        userInterface = new Group();
        userInterface.addActor(controller);
        userInterface.addActor(trigger);
        userInterface.addActor(nextWeaponButton);

        inputProcessor.addToInterface(controller);
        inputProcessor.addToInterface(trigger);


    }

    protected void update(double deltaT) {
        if(!hero.isAliveNow())
        {
            game.setScreen(ScreenType.MenuScreen);
        }

        if (hero.cellUpdated)
            field.getNeighbours(hero.Neighbours, hero.getCell());

        for (Actor b : bots.getActorsGroup()) {
            if (((Bot) b).cellUpdated)
                field.getNeighbours(((Bot) b).Neighbours, ((Bot) b).getCell());
            if (!((Bot) b).isAlive) {
                b.remove();
                score += 100;
                break;
            }
        }

        hero.update(deltaT);

        for (Actor b : bots.getActorsGroup()) {
            ((Bot) b).setDirection(hero.getPosition());
            ((Bot) b).update(deltaT);
        }

        for (Actor b : bullets.getActorsGroup()) {
            ((Bullet) b).update(deltaT);
        }

        for (Actor b : bullets.getActorsGroup()) {
            for (Actor a : bots.getActorsGroup())
            {
                ((Bullet) b).hitBot(((Bot) a));
            }
            if (((Bullet) b).cellUpdated)
                field.getNeighbours(((Bullet) b).Neighbours, ((Bullet) b).getCell());
            if (!((Bullet) b).getActive()) {
                b.remove();
                break;
            }
        }

        for (Actor b : bots.getActorsGroup())
        {
            fight(hero,(Bot) b);
        }


        spawnBot(deltaT);
    }

    protected void draw(Canvas canvas) {
        canvas.drawRect(0, 0, screenWidth, screenHeight, backgroundPaint);

        walls.draw(canvas);
        sceneObjects.draw(canvas);

        for (Actor b : bots.getActorsGroup())
            b.draw(canvas);
        userInterface.draw(canvas);
        bullets.draw(canvas);
        canvas.drawText(String.valueOf(score), 300, 300, fontPaint);
    }

    @Override
    public boolean isTouched() {
        return false;
    }

    public boolean touchDown(float x, float y) {
        nextWeaponButton.touchDown(x,y);
        return false;
        //return controller.touchDown(x, y) || trigger.touchDown(x, y);
    }

    public boolean touchDrag(float x, float y) {
        return false;
        //return controller.touchDrag(x, y);
    }

    public boolean touchUp(float x, float y) {
        return controller.touchUp(x, y) || trigger.touchUp(x, y);
    }

    public void nextWave() {
        waveSize += 10;
    }

    public void spawnBot(double deltaT) {
        cd += deltaT;
        if (bots.getActorsGroup().size() < maxSpawn)
            if (cd > 1) {
                bots.addActor(new Bot(200, 300, (float) (Values.getBlockMinSize()/2.5)));
                spawned++;
                cd = 0;
            }
    }
    public void fight(MainHero hero, Bot bot)
    {
        if(Overlapers.Overlaps(hero.getFigure(),bot.getFigure()))
        {
            if( (hero.hitAble))
            {
                hero.setLife(hero.getLife()-1);
                hero.hitAble=false;
            }

        }
    }

}

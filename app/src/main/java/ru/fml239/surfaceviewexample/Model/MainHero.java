package ru.fml239.surfaceviewexample.Model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;

import ru.fml239.framework.math.Circle;
import ru.fml239.framework.scene.Stage.Drawable;
import ru.fml239.surfaceviewexample.Utils.Values;
import ru.fml239.framework.math.Vector2;
import ru.fml239.framework.scene.Stage.Actor;
import ru.fml239.framework.scene.Stage.Group;

public class MainHero extends Actor implements Drawable {

    private int Life;
    private Bitmap img;
    private final double SPEED;
    private Paint paint;
    private Paint paint1;
    private float speedMultiplier;
    public boolean isAvailableX;
    public boolean isAvailableY;
    private Vector2 cell;
    public boolean cellUpdated;
    public Group Neighbours;
    private double rotation;
    private double direction;
    private Group bulletsGroup;
    private Bitmap bitmap;
    private boolean isShooting;
    public boolean hitAble;
    public Weapon weapon;
    private double cd1;
    private float lastDx;
    private float lastDy;

    public enum Weapon {
        Pistol, Shotgun, AK_47
    }

    private double cd;

    public int AK_47_Ammo;
    public int Shotgun_Ammo;

    public void setShooting(boolean bool) {
        isShooting = bool;
    }


    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }


    public enum heroState {
        Shooting, Running, Stand;
    }

    public void setLife(int i) {
        Life = i;
    }

    public int getLife() {
        return Life;
    }

    public void setDirection(double direction) {
        this.direction = direction;
    }

    public MainHero(float x, float y, float radius) {
        super(x, y, radius);
        Life = 10;
        SPEED = Values.getBlockMinSize() * 15;
        paint = new Paint();
        paint1 = new Paint();
        paint.setColor(Color.BLUE);
        paint1.setColor(Color.RED);
        Neighbours = new Group();
        cell = new Vector2();
        cellUpdated = true;
        isAvailableX = false;
        isAvailableY = false;
        rotation = 0;
        isShooting = false;
        weapon = Weapon.AK_47;
        AK_47_Ammo = 100;
        Shotgun_Ammo = 100;
        hitAble = true;
        img = setImage(Values.textures.get("battle.png"), getFigure().getWidth(), getFigure().getWidth());
    }

    public void update(double deltaT) {

        isAvailableX = true;
        isAvailableY = true;
        for (Actor a : Neighbours.getActorsGroup()) {
            isAvailableX &= !getFigure().overlapsNextX((float) deltaT * getSpeedX(), a.getFigure());
            isAvailableY &= !getFigure().overlapsNextY((float) deltaT * getSpeedY(), a.getFigure());
        }
        if (isAvailableX) moveBy(getSpeedX() * (float) deltaT, 0);
        if (isAvailableY) moveBy(0, getSpeedY() * (float) deltaT);

        updateCell();
        //rotation += 0.1f;
        //rotation %= Math.PI * 2;
        if (isShooting)
            spawnBullet(deltaT);
        isHitAble(deltaT);
        Wink();
    }


    public void setSpeedMultiplier(float speedMultiplier) {
        this.speedMultiplier = speedMultiplier;
    }

    @Override
    public void draw(Canvas canvas) {
        if (img != null) {
            Matrix m = new Matrix();

            m.setTranslate(getX() - getFigure().getWidth() / 2, getY() - getFigure().getWidth() / 2);
            m.preRotate((float) (direction * 180 / Math.PI + 90), getFigure().getWidth() / 2, getFigure().getWidth() / 2);

            canvas.drawBitmap(img, m, null);
        } else {
            paint.setColor(Color.BLACK);
            canvas.drawCircle(getX(), getY(), ((Circle) getFigure()).getRadius(), paint);
            canvas.drawCircle(getX(), getY(), ((Circle) getFigure()).getRadius() / 2, paint1);
            canvas.drawCircle(
                    getX() + 4 * ((Circle) getFigure()).getRadius() * (float) Math.cos(rotation),
                    getY() + 4 * ((Circle) getFigure()).getRadius() * (float) Math.sin(rotation),
                    getWidth() / 8,
                    paint1);
            //canvas.drawBitmap(bitmap, getLeft(), getTop(), paint);
        }
    }

    public float getSpeedX() {
        return (float) (SPEED * Math.cos(direction) * speedMultiplier);
    }

    public float getSpeedY() {
        return (float) (SPEED * Math.sin(direction) * speedMultiplier);
    }

    private void updateCell() {
        int newX = (int) (getPosition().x / Values.PPU_X);
        int newY = (int) (getPosition().y / Values.PPU_Y);
        cellUpdated = (((int) cell.x) != newX || ((int) cell.y) != newY);
        if (cellUpdated)
            cell.setCoords(newX, newY);
    }

    public Vector2 getCell() {
        cellUpdated = false;
        return cell;
    }

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public Group getBulletsGroup() {
        return bulletsGroup;
    }

    public void setBulletsGroup(Group bulletsGroup) {
        this.bulletsGroup = bulletsGroup;
    }

    public void spawnBullet(double deltaT) {
        cd += deltaT;
        if (weapon == Weapon.AK_47)
            if (AK_47_Ammo > 0) {
                if (cd >= 0.2) {
                    bulletsGroup.addActor(new Bullet(getPosition().x, getPosition().y, Values.getBlockMinSize() / 2, Values.getBlockMinSize() / 2, (float) direction, 100));
                    AK_47_Ammo--;
                    cd = 0;
                }
            } else weapon = Weapon.Shotgun;
        if (weapon == Weapon.Shotgun) {

            if (Shotgun_Ammo > 0) {
                if (cd >= 0.4) {
                    bulletsGroup.addActor(new Bullet(getPosition().x, getPosition().y, Values.getBlockMinSize() / 2, Values.getBlockMinSize() / 2, (float) direction, 100));
                    bulletsGroup.addActor(new Bullet(getPosition().x, getPosition().y, Values.getBlockMinSize() / 2, Values.getBlockMinSize() / 2, (float) (direction + Math.PI / 14), 100));
                    bulletsGroup.addActor(new Bullet(getPosition().x, getPosition().y, Values.getBlockMinSize() / 2, Values.getBlockMinSize() / 2, (float) (direction - Math.PI / 14), 100));
                    // bulletsGroup.addActor(new Bullet((float) (getPosition().x + 0.5*circle.getRadius() * (float) Math.cos(direction)), (float) (position.y +  0.5*circle.getRadius() * (float) Math.sin(direction)  ), screenWidth, screenHeight, (float) (direction-Math.PI/14)));
                    // bulletsGroup.addActor(new Bullet((float) (getPosition().x + 0.5*circle.getRadius() * (float) Math.cos(direction)), (float) (position.y +  0.5*circle.getRadius() * (float) Math.sin(direction)  ), screenWidth, screenHeight, (float) (direction+Math.PI/14)));
                    Shotgun_Ammo--;
                    cd = 0;
                }
            } else weapon = Weapon.Pistol;
        }
        if (weapon == Weapon.Pistol) {
            bulletsGroup.addActor(new Bullet(getPosition().x, getPosition().y, Values.getBlockMinSize() / 2, Values.getBlockMinSize() / 2, (float) direction, 100));
            isShooting = false;
        }
    }

    public boolean isAliveNow() {
        if (Life > 0)
            return true;
        else
            return false;
    }

    public void isHitAble(double deltaT) {
        if (!hitAble)
            cd1 += deltaT;

        if (cd1 > 1) {
            hitAble = true;
            cd1 = 0;
        }
    }

    public void NextWeapon() {
        switch (weapon) {
            case Pistol:
                weapon = Weapon.AK_47;
                break;
            case AK_47:
                weapon = Weapon.Shotgun;
                break;
            case Shotgun:
                weapon = Weapon.Pistol;
                break;
        }
    }

    public Bitmap setImage(Bitmap texture, float width, float height) {
        img = texture;
        img = Bitmap.createScaledBitmap(img, (int) width, (int) height, false);

        return img;
    }

    private void Wink() {
        if (!hitAble)

            img = setImage(Values.textures.get("battle1.png"), getFigure().getWidth(), getFigure().getWidth());
        else

            img = setImage(Values.textures.get("battle.png"), getFigure().getWidth(), getFigure().getWidth());
    }
}

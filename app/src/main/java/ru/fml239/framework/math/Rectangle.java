package ru.fml239.framework.math;

import android.util.Log;

/**
 * Created by Gleb on 17.04.2016.
 */
public class Rectangle extends GeometryFigure {
    public float height;
    public float width;

    public Rectangle(float x, float y, float width, float height) {
        super(FigureType.Rectangle, x, y);
        this.width = width;
        this.height = height;
    }

    public boolean Overlaps(Rectangle r) {
        return x <= r.x + r.width && r.x <= x + width && y <= r.y + r.height && r.y <= y + height;
    }

    public boolean Overlaps(Circle c) {
        if (topBorder() >= c.y) { //сверху
            if (leftBorder() >= c.x) //слева
                return (c.distance(leftBorder(), topBorder()) < c.radius);
            if (rightBorder() <= c.x) //справа
                return (c.distance(rightBorder(), topBorder()) < c.radius);
            return (topBorder() <= c.bottomBorder()); // посередине
        }
        if (bottomBorder() <= c.y) { //снизу
            if (leftBorder() >= c.x) //слева
                return (c.distance(leftBorder(), bottomBorder()) < c.radius);
            if (rightBorder() <= c.x) //справа
                return (c.distance(rightBorder(), bottomBorder()) < c.radius);
            return (bottomBorder() >= c.topBorder()); // посередине
        }
        if (leftBorder() >= c.x)
            return (leftBorder() <= c.rightBorder());
        if (rightBorder() <= c.x)
            return (rightBorder() >= c.leftBorder());

        if ((leftBorder() <= c.x) && (rightBorder() >= c.x))
            return bottomBorder() >= c.y || topBorder() <= c.y;
        return false;
    }

    public float leftBorder() {
        return x;
    }

    public float rightBorder() {
        return x + width;
    }

    public float topBorder() {
        return y;
    }

    public float bottomBorder() {
        return y + height;
    }

    @Override
    public float getWidth() {
        return width;
    }

    @Override
    public float getHeight() {
        return height;
    }

    @Override
    public void setWidth(float width) {
        this.width = width;
    }

    @Override
    public void setHeight(float height) {
        this.height = height;
    }

    @Override
    public void setSize(float width, float height) {
        setWidth(width);
        setHeight(height);
    }
    //TODO: метод boolean Overlaps(Circle) - пересекается ли с окружностью или нет
    //TODO: метод boolean isInside(Rectangle) - находится ли внутри другого прямоугольника
}

package ru.fml239.surfaceviewexample.Model;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import ru.fml239.framework.math.Overlapers;
import ru.fml239.framework.scene.Stage.Drawable;
import ru.fml239.surfaceviewexample.Utils.Values;
import ru.fml239.framework.math.Vector2;
import ru.fml239.framework.scene.Stage.Actor;
import ru.fml239.framework.scene.Stage.Group;

/**
 * Created by Gleb on 24.04.2016.
 */
public class Bullet extends Actor implements Drawable {
    private float direction;
    private float speed;
    public Paint paint;
    private boolean isActive;

    public boolean getActive() {
        return isActive;
    }

    private Vector2 cell;
    public boolean cellUpdated;
    public Group Neighbours;

    public Bullet(float x, float y, float width, float height, float direction, float speed) {
        super(x - width / 2, y - width / 2, width, height);
        this.direction = direction;
        this.speed = speed;
        paint = new Paint(Color.CYAN);
        Neighbours = new Group();
        cell = new Vector2();
        cellUpdated = true;
        isActive = true;

    }

    public void update(double deltaT) {

        for (Actor a : Neighbours.getActorsGroup()) {
            if (Overlapers.Overlaps(this.getFigure(), a.getFigure()))
                isActive = false;
        }
        moveBy((float) (getSpeedX() * deltaT), (float) (getSpeedY() * deltaT));

        updateCell();
    }

    public float getSpeedX() {
        return (float) (speed * Math.cos(direction));
    }

    public float getSpeedY() {
        return (float) (speed * Math.sin(direction));
    }

    @Override
    public void draw(Canvas canvas) {
        //canvas.rotate((float) (Math.toDegrees(direction)));
        canvas.drawRect(getX(), getY(), getX() + getWidth(), getY() + getHeight(), paint);
        //canvas.rotate((float) (-Math.toDegrees(direction)));
    }

    private void updateCell() {
        int newX = (int) (getPosition().x / Values.PPU_X);
        int newY = (int) (getPosition().y / Values.PPU_Y);
        cellUpdated = (((int) cell.x) != newX || ((int) cell.y) != newY);
        if (cellUpdated)
            cell.setCoords(newX, newY);
    }

    public Vector2 getCell() {
        cellUpdated = false;
        return cell;
    }


    public void hitBot(Bot b) {
        if (Overlapers.Overlaps(this.getFigure(), b.getFigure())) {
            b.hit();
            isActive = false;
            this.remove();
        }
    }
}

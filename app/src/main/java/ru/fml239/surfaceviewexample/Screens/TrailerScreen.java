package ru.fml239.surfaceviewexample.Screens;

import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.IOException;
import java.io.InputStream;

import ru.fml239.framework.scene.Stage.Screen;
import ru.fml239.surfaceviewexample.Game;
import ru.fml239.surfaceviewexample.R;

/**
 * Created by Gleb on 21.05.2016.
 */
public class TrailerScreen extends Screen {
    private MediaPlayer sound;
    private InputStream stream;
    private Movie movie;
    private  float movieWidth;
    private  float movieHeight;
    private  long movieDuration;
    private  long movieStart;
    public TrailerScreen(Game game, SurfaceHolder surfaceHolder, int screenWidth, int screenHeight, AssetManager assetManager) {
        super(game, surfaceHolder, screenWidth, screenHeight);
        try {
            stream = assetManager.open("1.gif");
        } catch (IOException e) {
            e.printStackTrace();
        }
        sound = MediaPlayer.create(game.getContext(), R.raw.trailer);
        movie = Movie.decodeStream(stream);
        movieWidth = movie.width();
        movieHeight = movie.height();
        movieDuration = movie.duration();
    }

    @Override
    protected void update(double deltaT)
    {}


    @Override
    protected void draw(Canvas canvas)
    {   sound.start();
        long now = SystemClock.uptimeMillis();
        if(movieStart==0)
        {
            movieStart=now;
        }

        if(movie!=null)
        {
            int dur = movie.duration();
            if(dur==0)
            {
                dur=1000;
            }
            int relTime = (int)((now - movieStart)%dur);
            movie.setTime(relTime);

            movie.draw(canvas,screenWidth/4,screenHeight/4);
        }

    }

    public boolean isTouched() {
        return false;
    }

    @Override
    public boolean touchDown(float x, float y) {
        Log.d("a", "a");sound.stop();
        game.setScreen(ScreenType.MenuScreen);

        return false;
    }

    @Override
    public boolean touchDrag(float x, float y) {
        return false;
    }

    @Override
    public boolean touchUp(float x, float y) {
        return false;
    }
}
package ru.fml239.framework.math;

/**
 * Created by Gleb on 22.04.2016.
 */
public class Circle extends GeometryFigure {
    float radius;

    public Circle(float x, float y, float radius) {
        super(FigureType.Circle, x, y);
        this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }

    @Override
    public float leftBorder() {
        return x - radius;
    }

    public float rightBorder() {
        return x + radius;
    }

    public float topBorder() {
        return y - radius;
    }

    public float bottomBorder() {
        return y + radius;
    }

    public float getWidth() {
        return radius * 2;
    }

    public float getHeight() {
        return radius * 2;
    }

    public void setWidth(float width) {
        radius = width / 2;
    }

    public void setHeight(float height) {
        radius = height / 2;
    }

    public void setSize(float width, float height) {
        float tmp = Math.min(width, height);
        setWidth(tmp);
        setHeight(tmp);
    }
}

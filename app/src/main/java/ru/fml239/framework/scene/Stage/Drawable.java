package ru.fml239.framework.scene.Stage;

import android.graphics.Canvas;

/**
 * Created by Gleb on 17.04.2016.
 */
public interface Drawable {
    void draw(Canvas canvas);
}

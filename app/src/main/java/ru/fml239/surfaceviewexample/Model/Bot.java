package ru.fml239.surfaceviewexample.Model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;

import ru.fml239.framework.scene.Stage.Drawable;
import ru.fml239.surfaceviewexample.Utils.Values;
import ru.fml239.framework.math.Vector2;
import ru.fml239.framework.scene.Stage.Actor;
import ru.fml239.framework.scene.Stage.Group;

/**
 * Created by Gleb on 19.04.2016.
 */
public class Bot extends Actor implements Drawable {
    Paint paint;
    public float direction;
    private float speedx;
    private float speedy;
    private final double SPEED = 100f;
    public int hp;

    public boolean isAlive;
    private Vector2 cell;
    public boolean cellUpdated;
    public boolean isAvailableX;
    public boolean isAvailableY;
    public Group Neighbours;
    public boolean readyToFight;
    private int cd1;
    private Bitmap img;

    public Bot(float x, float y, float width) {
        super(x - width, y - width, width * 2, width * 2);
        hp = 2;
        Neighbours = new Group();
        cell = new Vector2();
        cellUpdated = true;
        paint = new Paint();
        paint.setColor(Color.BLUE);
        isAlive = true;
        //img = setImage(Values.textures.get("tomat.png"), getWidth(), getWidth());
    }

    public void update(double deltaT) {
        if (isAlive) {
            readyToFightNow(deltaT);
            isAvailableX = true;
            isAvailableY = true;
            speedx = (float) SPEED * (float) Math.cos(direction);
            speedy = (float) SPEED * (float) Math.sin(direction);

            for (Actor a : Neighbours.getActorsGroup()) {
                isAvailableX &= !getFigure().overlapsNextX((float) deltaT * getSpeedX(), a.getFigure());
                isAvailableY &= !getFigure().overlapsNextY((float) deltaT * getSpeedY(), a.getFigure());
            }
            if (isAvailableY)
                moveBy(0, (float) (speedy * deltaT));
            if (isAvailableX)
                moveBy((float) (speedx * deltaT), 0);

            if (hp <= 0) {
                paint.setColor(Color.RED);
                isAlive = false;
            }
            updateCell();
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if (img != null) {
            Matrix m = new Matrix();

            m.setTranslate(getX() - getFigure().getWidth() / 2, getY() - getFigure().getWidth() / 2);
            m.preRotate((float) (direction * 180 / Math.PI + 90), getFigure().getWidth() / 2, getFigure().getWidth() / 2);

            canvas.drawBitmap(img, m, null);
        } else {
            canvas.drawCircle(getX() + getWidth() / 2, getY() + getWidth() / 2, getWidth() / 2, paint);
        }
    }

    public void setDirection(Vector2 position) {
        direction = getPosition().angle(position);
    }

    private void updateCell() {
        int newX = (int) (getPosition().x / Values.PPU_X);
        int newY = (int) (getPosition().y / Values.PPU_Y);
        cellUpdated = (((int) cell.x) != newX || ((int) cell.y) != newY);
        if (cellUpdated)
            cell.setCoords(newX, newY);
    }

    public Vector2 getCell() {
        cellUpdated = false;
        return cell;
    }

    public float getSpeedY() {
        return speedy;
    }

    public float getSpeedX() {
        return speedx;
    }

    public void hit() {
        hp--;
    }

    public boolean getAlive() {
        return isAlive;
    }


    public void readyToFightNow(double deltaT) {
        if (!readyToFight)
            cd1 += deltaT;
        if (cd1 > 2) {
            readyToFight = true;
            cd1 = 0;
        }
    }

    public Bitmap setImage(Bitmap texture, float width, float height) {
        img = texture;
        img = Bitmap.createScaledBitmap(img, (int) width, (int) height, false);

        return img;
    }
}

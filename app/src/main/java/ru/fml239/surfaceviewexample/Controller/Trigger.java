package ru.fml239.surfaceviewexample.Controller;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import ru.fml239.surfaceviewexample.Model.MainHero;
import ru.fml239.framework.scene.Stage.Drawable;
import ru.fml239.framework.scene.Touch.Touchable;
import ru.fml239.framework.scene.Stage.Actor;

/**
 * Created by Gleb on 24.04.2016.
 */

public class Trigger extends Actor implements Drawable, Touchable {
    private float r;
    private float rotation;
    private Paint backgroundPaint;
    private boolean touched;
    private MainHero hero;

    public Trigger(MainHero hero)
    {
        super(350, 1500, 300, 300);
        r = getWidth() / 2;
        this.hero = hero;
        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.BLUE);
    }

    @Override
    protected Actor hit(float x, float y) {
        if (getPosition().distance(x, y) < r)
            return this;
        else
            return null;
    }
    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(getPosition().x, getPosition().y, r, backgroundPaint);
    }
    @Override
    public boolean isTouched() {
        return touched;
    }

    @Override
    public boolean touchDown(float x, float y) {
        touched = hit(x, y)!=null;
        if(touched)
        hero.setShooting(true);
        return false;
    }

    public void setSize(float width, float height) {
        super.setSize(width, height);
        r = width / 2;
    }
    @Override
    public boolean touchDrag(float x, float y) {
        return false;
    }

    @Override
    public boolean touchUp(float x, float y) {

        touched = false;

        hero.setShooting(false);
        return touched;
    }




}

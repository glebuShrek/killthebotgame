package ru.fml239.framework.scene.Stage;

import android.graphics.Canvas;

import java.util.ArrayList;

/**
 * Created by Gleb on 20.04.2016.
 */
public class Group extends Actor {
    private ArrayList<Actor> actorsGroup;

    public Group() {
        super(0, 0, 0, 0);
        actorsGroup = new ArrayList<>();
    }

    public void addActor(Actor actor) {
        if(actor == null)
            return;
        if (actorsGroup.isEmpty()) {
            actorsGroup.add(actor);
            setPosition(actor.getX(), actor.getY());
            setSize(actor.getWidth(), actor.getHeight());
        } else {
            actorsGroup.add(actor);
            setPosition(Math.min(getLeft(), actor.getLeft()),
                    Math.min(getTop(), actor.getTop()));
            setSize(Math.max(getRight(), actor.getRight()) - getX(),
                    Math.min(getBottom(), actor.getBottom()) - getY());
        }
        actor.setGroup(this);
    }

    protected Actor hit(float x, float y) {
        if (super.hit(x, y) != null) {
            for (Actor a : actorsGroup)
                if (a.hit(x, y) != null)
                    return a;
        }
        return null;
    }

    public void moveBy(float dx, float dy) {
        super.moveBy(dx, dy);
        for (Actor a : actorsGroup)
            a.moveBy(dx, dy);
    }
    public void draw(Canvas canvas) {
        for(Actor a : actorsGroup)
            a.draw(canvas);
    }

    public void clearGroup(){
        actorsGroup.clear();
    }

    public ArrayList<Actor> getActorsGroup() {
        return actorsGroup;
    }
    public boolean remove(Actor actor){
        return actorsGroup.remove(actor);

    }
}

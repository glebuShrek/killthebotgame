package ru.fml239.framework.math;

import android.content.res.Resources;
import android.util.Log;

import static ru.fml239.framework.math.FigureType.Circle;

/**
 * Created by Gleb on 03.05.2016.
 */
public class Overlapers {
    public static boolean Overlaps(Rectangle r1, Rectangle r2) {
        return r1.x <= r2.x + r2.width && r2.x <= r1.x + r1.width && r1.y <= r2.y + r2.height && r2.y <= r1.y + r1.height;
    }

    public static boolean Overlaps(Rectangle r, Circle c, boolean b) {
        if (r.topBorder() >= c.y) { //круг сверху
            if (r.leftBorder() >= c.x) //круг слева
                return (c.distance(r.leftBorder(), r.topBorder()) < c.radius);
            if (r.rightBorder() <= c.x) //круг справа
                return (c.distance(r.rightBorder(), r.topBorder()) < c.radius);
            return (r.topBorder() <= c.bottomBorder()); // круг посередине
        }
        if (r.bottomBorder() <= c.y) { //круг снизу
            if (r.leftBorder() >= c.x) //круг слева
                return (c.distance(r.leftBorder(), r.bottomBorder()) < c.radius);
            if (r.rightBorder() <= c.x) //справа
                return (c.distance(r.rightBorder(), r.bottomBorder()) < c.radius);
            return (r.bottomBorder() >= c.topBorder()); // посередине
        }
        if (r.leftBorder() >= c.x) //круг слева
            return (r.leftBorder() <= c.rightBorder());
        if (r.rightBorder() <= c.x) //круг справа
            return (r.rightBorder() >= c.leftBorder());

        if ((r.leftBorder() <= c.x) && (r.rightBorder() >= c.x))
            return r.bottomBorder() >= c.y || r.topBorder() <= c.y;
        return false;
    }

    public static boolean Overlaps(Rectangle r, Circle c) {
        float circleDistancex, circleDistancey,
                rectx = r.x + r.width/2,
                recty = r.y + r.height/2,
                rectwidth = r.width,
                rectheight = r.height,
                circlex = c.x,
                circley = c.y,
                circler = c.radius, cornerDistance_sq;

        circleDistancex = Math.abs(circlex - rectx);
        circleDistancey = Math.abs(circley - recty);

        if (circleDistancex > (rectwidth/2 + circler)) { return false; }
        if (circleDistancey > (rectheight/2 + circler)) { return false; }

        if (circleDistancex <= (rectwidth/2)) { return true; }
        if (circleDistancey <= (rectheight/2)) { return true; }

        cornerDistance_sq = (float) (Math.pow((circleDistancex - rectwidth/2),2) + Math.pow((circleDistancey - rectheight/2),2));

        return (cornerDistance_sq <= (Math.pow(circler,2)));
    }

    public static boolean Overlaps(Circle c1, Circle c2) {
        return (c1.distance(c2.getX(), c2.getY()) < c1.radius + c2.getRadius());
    }

    public static boolean Overlaps(GeometryFigure figure1, GeometryFigure figure2) {
        switch (figure1.figureType) {
            case Circle:
                switch (figure2.figureType) {
                    case Circle:
                        return Overlaps((Circle) figure1, (Circle) figure2);
                    case Rectangle:
                        return Overlaps((Rectangle) figure2, (Circle) figure1);
                }
                break;
            case Rectangle:
                switch (figure2.figureType) {
                    case Circle:
                        return Overlaps((Rectangle) figure1, (Circle) figure2);
                    case Rectangle:
                        return Overlaps((Rectangle) figure1, (Rectangle) figure2);
                }
                break;
        }
        return false;
    }
}

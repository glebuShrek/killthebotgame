package ru.fml239.surfaceviewexample.Controller;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import ru.fml239.surfaceviewexample.Model.MainHero;
import ru.fml239.framework.scene.Stage.Drawable;
import ru.fml239.framework.scene.Touch.Touchable;
import ru.fml239.framework.scene.Stage.Actor;

public class JoyStick extends Actor implements Touchable, Drawable {

    private float r;
    private float knobDistance;
    private float rotation;
    private MainHero hero;
    private Paint backgroundPaint;
    private Paint knobPaint;
    private boolean touched;

    public JoyStick(MainHero hero1) {
        super(150, 1500, 300, 300);
        r = getWidth() / 2;
        this.hero = hero1;
        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.BLUE);
        knobPaint = new Paint();
        knobPaint.setColor(Color.RED);
    }

    public boolean touch(float x, float y) {
        rotation = getPosition().angle(x, y);
        knobDistance = Math.min(getPosition().distance(x, y), r * 3 / 4);
        hero.setSpeedMultiplier(knobDistance / r);
        hero.setDirection(rotation);
        return true;
    }

    public void setSize(float width, float height) {
        super.setSize(width, height);
        r = width / 2;
    }

    @Override
    public Actor hit(float x, float y) {
        if (getPosition().distance(x, y) < r)
            return this;
        else
            return null;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(getPosition().x, getPosition().y, r, backgroundPaint);
        canvas.drawCircle(getPosition().x + knobDistance * (float) Math.cos(rotation), getPosition().y + knobDistance * (float) Math.sin(rotation), r / 4, knobPaint);
    }

    @Override
    public boolean isTouched() {
        return touched;
    }

    @Override
    public boolean touchDown(float x, float y) {
        touched = hit(x, y)!=null;
        return false;
    }

    @Override
    public boolean touchDrag(float x, float y) {
        if (isTouched())
            return touch(x, y);
        return false;
    }

    @Override
    public boolean touchUp(float x, float y) {
        touched = false;
        knobDistance = 0;
        hero.setSpeedMultiplier(0);
        return touched;
    }
}

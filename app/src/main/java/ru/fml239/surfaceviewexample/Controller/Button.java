package ru.fml239.surfaceviewexample.Controller;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;

import java.io.IOException;

import ru.fml239.framework.scene.Stage.Actor;
import ru.fml239.framework.scene.Touch.Touchable;
import ru.fml239.surfaceviewexample.Game;
import ru.fml239.surfaceviewexample.Screens.ScreenType;
import ru.fml239.surfaceviewexample.Utils.Values;

/**
 * Created by Gleb on 02.05.2016.
 */
public abstract class Button extends Actor implements Touchable {
    private Bitmap img;
    protected boolean touched;
    private Game game;

    public Button(float x, float y, float width, float height, Game game, Bitmap b) {
        super(x, y, width, height);
        img=setImage(b, width/8,width/8);
        setSize(img.getWidth(),img.getHeight());
        this.game = game;
    }

    @Override
    public void draw(Canvas canvas) {
        if (img != null) {

            canvas.drawBitmap(img, getPosition().x, getPosition().y, null);
        }
    }

    public Bitmap setImage( Bitmap texture,float width, float height) {
        img = texture;
        img = Bitmap.createScaledBitmap(img, (int) width, (int) height, false);

        return  img;
    }

    public abstract void act();

    @Override
    public boolean isTouched() {
        return touched;
    }

    @Override
    public boolean touchDown(float x, float y) {
        touched = (hit(x, y) != null);
        if (touched) act();
        return false;
    }

    @Override
    public boolean touchDrag(float x, float y) {
        return false;
    }

    @Override
    public boolean touchUp(float x, float y) {
        touched = false;
        return touched;
    }
}

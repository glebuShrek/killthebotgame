package ru.fml239.surfaceviewexample;

import android.content.res.AssetManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.HashMap;

import ru.fml239.framework.scene.Touch.Pointer;
import ru.fml239.surfaceviewexample.Screens.GameScreen;
import ru.fml239.surfaceviewexample.Screens.MenuScreen;
import ru.fml239.framework.scene.Stage.Screen;
import ru.fml239.surfaceviewexample.Screens.ScreenType;
import ru.fml239.surfaceviewexample.Screens.TrailerScreen;
import ru.fml239.surfaceviewexample.Utils.Values;

public class Game extends SurfaceView implements
        SurfaceHolder.Callback {
    private Screen currentScreen;
    private AssetManager assetManager;

    public Game(MainActivity activity) {
        super(activity);
        this.assetManager = activity.getAssets();
        getHolder().addCallback(this);
        Values.loadGraphics(assetManager);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("touchEvent", String.valueOf(event.getAction()));
        if (currentScreen.applyMotionEvent(event))
            return true;
        /*
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                currentScreen.touchDown(event.getX(), event.getY());
                return true;
            case MotionEvent.ACTION_MOVE:
                currentScreen.touchDrag(event.getX(), event.getY());
                return true;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                currentScreen.touchUp(event.getX(), event.getY());
                return false;
        }*/
        return super.onTouchEvent(event);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        setScreen(ScreenType.TrailerScreen);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        currentScreen.setRunning(false);
        while (retry) {
            try {
                currentScreen.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public void setScreen(ScreenType screen) {
        if (currentScreen != null)
            currentScreen.setRunning(false);
        switch (screen){
            case GameScreen:
                currentScreen = new GameScreen(this, getHolder(), getWidth(), getHeight(), assetManager);
                break;
            case MenuScreen:
                currentScreen = new MenuScreen(this, getHolder(), getWidth(), getHeight(), assetManager);
                break;
            case TrailerScreen:
                currentScreen= new TrailerScreen(this, getHolder(), getWidth(), getHeight(), assetManager);
                break;
        }
        currentScreen.setRunning(true);
        currentScreen.start();
    }

}

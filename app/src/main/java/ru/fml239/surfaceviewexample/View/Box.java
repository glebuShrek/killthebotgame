package ru.fml239.surfaceviewexample.View;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import ru.fml239.framework.scene.Stage.Actor;
import ru.fml239.framework.scene.Stage.Drawable;

/**
 * Created by Gleb on 15.04.2016.
 */
public class Box extends Actor implements Drawable {
    private Paint paint;

    public Box(float x, float y, float r, float r1) {
        super(x, y, r, r1);
        paint = new Paint();
        paint.setColor(Color.BLACK);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRect(getLeft(), getTop(), getRight(), getBottom(), paint);
    }
}

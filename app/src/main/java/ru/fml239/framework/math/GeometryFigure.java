package ru.fml239.framework.math;

/**
 * Created by Gleb on 03.05.2016.
 */
public abstract class GeometryFigure extends Vector2 {
    public FigureType figureType;

    public GeometryFigure(FigureType type, float x, float y) {
        super(x, y);
        this.figureType = type;
    }

    public boolean overlapsNextX(float x, GeometryFigure figure) {
        synchronized (this) {
            boolean result;
            moveBy(x, 0);
            result = Overlaps(figure);
            moveBy(-x, 0);
            return result;
        }
    }

    public boolean overlapsNextY(float y, GeometryFigure figure) {
        synchronized (this) {
            boolean result;
            moveBy(0, y);
            result = Overlaps(figure);
            moveBy(0, -y);
            return result;
        }
    }

    public boolean Overlaps(GeometryFigure figure) {return Overlapers.Overlaps(this, figure);}

    public abstract float leftBorder();

    public abstract float rightBorder();

    public abstract float topBorder();

    public abstract float bottomBorder();

    public abstract float getWidth();

    public abstract float getHeight();

    public abstract void setWidth(float width);

    public abstract void setHeight(float height);

    public abstract void setSize(float width, float height);

}

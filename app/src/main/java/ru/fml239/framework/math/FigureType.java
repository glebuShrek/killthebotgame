package ru.fml239.framework.math;

/**
 * Created by Gleb on 03.05.2016.
 */
public enum FigureType {
    Circle,
    Rectangle
}

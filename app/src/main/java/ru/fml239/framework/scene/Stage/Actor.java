package ru.fml239.framework.scene.Stage;

import ru.fml239.framework.math.Circle;
import ru.fml239.framework.math.GeometryFigure;
import ru.fml239.framework.math.Rectangle;
import ru.fml239.framework.math.Vector2;

/**
 * Created by Gleb on 16.04.2016.
 */
public abstract class Actor implements Drawable {
    protected GeometryFigure figure;
    private Group group;

    public Actor(float x, float y, float width, float height) {
        figure = new Rectangle(x, y, width, height);
    }

    public Actor(float x, float y, float radius) {
        figure = new Circle(x, y, radius);
    }

    public Vector2 getPosition() {
        return figure;
    }

    public float getX() {
        return figure.x;
    }

    public float getY() {
        return figure.y;
    }

    public float getWidth() {
        return figure.getWidth();
    }

    public float getHeight() {
        return figure.getHeight();
    }

    public float getLeft() {
        return figure.leftBorder();
    }

    public float getRight() {
        return figure.rightBorder();
    }

    public float getTop() {
        return figure.topBorder();
    }

    public float getBottom() {
        return figure.bottomBorder();
    }

    protected Actor hit(float x, float y) { //возвращает TRUE, если координаты нажатия попали по актеру
        if ((x - getX()) > 0 && (y - getY() > 0) && (x - getX() < getWidth()) && (y - getY() < getHeight()))
            return this;
        else
            return null;
    }

    public void setSize(float width, float height) {
        figure.setSize(width, height);
    }

    public void setPosition(float x, float y) {
        figure.x = x;
        figure.y = y;
    }

    public void moveBy(float dx, float dy) {
        figure.moveBy(dx, dy);
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public boolean remove() {
        if (group == null)
            return false;
        return group.remove(this);
    }

    public boolean overlapsNextX(float v, GeometryFigure figure) {
        return this.figure.overlapsNextX(v, figure);
    }

    public boolean overlapsNextY(float v, GeometryFigure figure) {
        return this.figure.overlapsNextY(v, figure);
    }

    public GeometryFigure getFigure() {
        return figure;
    }
}

package ru.fml239.framework.scene.Touch;

import android.util.Log;
import android.view.InputEvent;
import android.view.MotionEvent;

import java.util.HashMap;
import java.util.HashSet;

import ru.fml239.framework.scene.Stage.Screen;

/**
 * Created by Gleb on 27.04.2016.
 */
public class InputProcessor {

    private HashMap<Integer, Pointer> pressedPointers;
    private HashSet<Touchable> inputInterface;
    private Screen parentScreen;

    public InputProcessor(Screen screen) {
        pressedPointers = new HashMap<>();
        inputInterface = new HashSet<>();
        parentScreen = screen;
    }

    public void addToInterface(Touchable touchable) {
        inputInterface.add(touchable);
    }

    public void act() {
        for (Pointer p : pressedPointers.values()) {
            switch (p.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    for (Touchable t : inputInterface)
                        t.touchDown(p.getX(), p.getY());
                    break;
                case MotionEvent.ACTION_MOVE:
                    Log.d("Move", String.format("count: %d", inputInterface.size()));
                    for (Touchable t : inputInterface) {
                        Log.d("Coords", String.format("%f %f", p.getX(), p.getY()));
                        t.touchDrag(p.getX(), p.getY());
                    }
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    for (Touchable t : inputInterface)
                        t.touchUp(p.getX(), p.getY());
                    break;
            }
        }
    }

    public boolean processMotionEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                Log.d("Down", "h");
                parentScreen.touchDown(event.getX(), event.getY());
                return putPointer(event);
            case MotionEvent.ACTION_MOVE:
                Log.d("Move", "h");
                parentScreen.touchDrag(event.getX(), event.getY());
                return dragPointer(event);
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                parentScreen.touchUp(event.getX(), event.getY());
                Log.d("Up", "h");
                return removePointer(event);
        }
        return false;
    }

    public boolean putPointer(MotionEvent event) {
        pressedPointers.put(event.getActionIndex(), new Pointer(event));
        return true;
    }

    public boolean dragPointer(MotionEvent event) {
        Log.d("in", "dragPointer");
        if (!pressedPointers.containsKey(event.getActionIndex()))
            return false;
        Log.d("do", "dragPointer");
        pressedPointers.get(event.getActionIndex()).move(event);
        return true;
    }

    public boolean removePointer(MotionEvent event) {
        if (!pressedPointers.containsKey(event.getActionIndex()))
            return false;
        pressedPointers.remove(event.getActionIndex());
        return true;
    }
}

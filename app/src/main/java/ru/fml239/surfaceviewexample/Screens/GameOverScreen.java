package ru.fml239.surfaceviewexample.Screens;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

import ru.fml239.framework.scene.Stage.Screen;
import ru.fml239.surfaceviewexample.Controller.Button;
import ru.fml239.surfaceviewexample.Game;
import ru.fml239.surfaceviewexample.Utils.Values;

/**
 * Created by Gleb on 24.05.2016.
 */

public class GameOverScreen extends Screen {
    private Bitmap img;
    private Button playButton;

    public GameOverScreen(Game game, SurfaceHolder surfaceHolder, int screenWidth, int screenHeight, AssetManager assetManager) {
        super(game, surfaceHolder, screenWidth, screenHeight);
        //playButton = new Button(screenWidth / 3, screenHeight / 3, 5, 5, game ,Values.textures.get("tomat.png"));
        inputProcessor.addToInterface(playButton);
    }

    @Override
    protected void draw(Canvas canvas) {
        playButton.draw(canvas);
    }

    @Override
    protected void update(double deltaT) {

    }

    @Override
    public boolean isTouched() {
        return false;
    }

    @Override
    public boolean touchDown(float x, float y) {
        playButton.touchDown(x, y);
        if (playButton.isTouched())
            game.setScreen(ScreenType.GameScreen);
        return false;
    }

    @Override
    public boolean touchDrag(float x, float y) {
        return false;
    }

    @Override
    public boolean touchUp(float x, float y) {
        return false;
    }
}
package ru.fml239.framework.math;

/**
 * Created by Gleb on 17.04.2016.
 */
public class Vector2 {
    public float x;
    public float y;

    public Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2() {
        this(0, 0);
    }

    public void setCoords(float x, float y){
        this.x = x;
        this.y = y;
    }

    public float length() {
        return (float) Math.sqrt(x * x + y * y);
    }

    public float angle() {
        return (float) Math.atan2(y, this.x);
    }

    public Vector2 difference(Vector2 sub) {
        return new Vector2(this.x - sub.x, this.y - sub.y);
    }

    public Vector2 summ(Vector2 sum) {
        return new Vector2(this.x + sum.x, this.y + sum.y);
    }

    public Vector2 mult(float alpha) {
        return new Vector2(this.x * alpha, this.y * alpha);
    }

    public void subtract(Vector2 sub) {
        x -= sub.x;
        y -= sub.y;
    }

    public void add(Vector2 add) {
        x += add.x;
        y += add.y;
    }

    public void multiply(float a) {
        x *= a;
        y *= a;
    }

    public float distance(Vector2 to) {
        return (float) Math.sqrt((to.x-x)*(to.x-x) + (to.y-y)*(to.y-y));
    }
    public float distance(float toX, float toY) {
        return (float) Math.sqrt((toX-x)*(toX-x) + (toY-y)*(toY-y));
    }

    public float angle(Vector2 from) {
        return (float) Math.atan2(from.y - y, from.x - x);
    }

    public float angle(float fromX, float fromY) {
        return (float) Math.atan2(fromY - y, fromX - x);
    }

    public void setLength(float length){
        multiply(length/length());
    }

    public void setAngle(float angle){
        float length = length();
        setCoords((float)Math.cos(angle)*length, (float)Math.sin(angle)*length);
    }

    public void rotateBy(float antgle){
        setAngle(angle()+antgle);
    }
    public void moveBy(float dx, float dy){ x+=dx; y+=dy;}
    public float getX() {
        return x;
    }
    public float getY() {
        return y;
    }

    public String toString(){
        return String.format("(%f;%f)", x, y);
    }

}
